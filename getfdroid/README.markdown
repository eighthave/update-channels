F-Droid Installer
=================
This library contains `org.fdroid.getfdroid.FDroidInstaller`, a Java
class to help you detect whether a valid copy of the Play Store or F-Droid are
installed, and to help you install a copy of F-Droid.

Usage: Detection
----------------
Create an instance of `FDroidInstaller` and call these methods:

- `getPlayStoreStatus()`, to determine if a valid copy of the Play Store is
installed

- `getFDroidStatus()`, to determine if a valid copy of F-Droid is installed

- `isFDroidNeeded()`, to determine if a copy of F-Droid might be needed

The first two methods return a `PackageStatus` enum, with three possible
values:

- `VALID`, meaning that the app in question is installed and has the expected
signature

- `INVALID`, meaning that the app in question is installed but it failed
verification checks (e.g., has the wrong signature, suggesting a hacked version
of the app)

- `NOT_INSTALLED`, meaning that the app in question is not installed

`isFDroidNeeded()` returns a simple `boolean`. `false` means that a valid
copy of the Play Store or a valid copy of F-Droid was detected. In general,
favor the other methods, as the `PackageStatus` gives you more information about
the state of the system. For example, if F-Droid is installed, but it has an
invalid signature, `isFDroidNeeded()` returns `true`, but you cannot actually
install a good copy of F-Droid, since a bad copy is already installed.

Usage: Download
---------------
On an `FDroidInstaller` instance, call `download()` to request that a copy
of F-Droid be downloaded. This method
takes a `FileDownloadStrategy` and a `FileDownloadStrategy.Callback` as parameters.
The documentation on [the `common/` module](../../common/README.markdown)
has details on using those classes.

When your callback is told that the download is successful, you can proceed
to install F-Droid, if desired.

Usage: Installation
-------------------
Given a successful download of F-Droid via the `download()` methods, call
`install()` to kick off installation of F-Droid from the downloaded copy.
This returns a boolean, `true` if installation has started, `false` otherwise
(e.g., we cannot find a valid copy of F-Droid to install).

Alternatively, call `buildPendingIntent()` to create a `PendingIntent`
that will kick off installation of F-Droid from the downloaded copy. You might
use this from a `Notification`, for example. This will return `null` if we
cannot find a valid copy of F-Droid to install.

When you are done with the installation, ideally call `cleanup()` to get
rid of the downloaded F-Droid APK.

Demo
----
The `getfdroid-demo/` module contains an Android app that uses `FDroidInstaller`
to check the installation status of the Play Store and F-Droid and, if needed,
offers an overflow menu option to install F-Droid. That, in turn, has a submenu
for two possible download strategies: OkHttp and "HURL" (`HttpURLConnection`).