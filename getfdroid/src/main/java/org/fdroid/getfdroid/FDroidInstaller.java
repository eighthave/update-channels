/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.getfdroid;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import org.fdroid.apputil.common.FileDownloadStrategy;
import org.spongycastle.openpgp.PGPCompressedData;
import org.spongycastle.openpgp.PGPException;
import org.spongycastle.openpgp.PGPPublicKey;
import org.spongycastle.openpgp.PGPPublicKeyRingCollection;
import org.spongycastle.openpgp.PGPSignature;
import org.spongycastle.openpgp.PGPSignatureList;
import org.spongycastle.openpgp.PGPUtil;
import org.spongycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.spongycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.spongycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Helper class for installing F-Droid from an app.
 * <p>
 * Use getPlayStoreStatus() and getFdroidStatus() to see if either major
 * distribution channel's app is installed and has the proper signature.
 * <p>
 * Use download() to download the F-Droid APK. Then, use install() to
 * install it or buildPendingIntent() to create a PendingIntent to trigger
 * installation (e.g., from a Notification). If you use install(), call
 * cleanup() when you think it is safe to delete the APK.
 */
public class FDroidInstaller {
    static final String SUBDIR_FDROID = ".fdroid-getfdroid";
    static final String FILENAME_FDROID = "FDroid.apk";
    static final String FILENAME_FDROID_ASC = "FDroid.apk.asc";
    static final String MIME_TYPE_APK =
            "application/vnd.android.package-archive";
    private static final String PACKAGE_PLAY_STORE = "com.android.vending";
    private static final String HASH_PLAY_STORE =
            "308204433082032ba003020102020900c2e08746644a308d300d06092a864886f70d01010405003074310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e205669657731143012060355040a130b476f6f676c6520496e632e3110300e060355040b1307416e64726f69643110300e06035504031307416e64726f6964301e170d3038303832313233313333345a170d3336303130373233313333345a3074310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e205669657731143012060355040a130b476f6f676c6520496e632e3110300e060355040b1307416e64726f69643110300e06035504031307416e64726f696430820120300d06092a864886f70d01010105000382010d00308201080282010100ab562e00d83ba208ae0a966f124e29da11f2ab56d08f58e2cca91303e9b754d372f640a71b1dcb130967624e4656a7776a92193db2e5bfb724a91e77188b0e6a47a43b33d9609b77183145ccdf7b2e586674c9e1565b1f4c6a5955bff251a63dabf9c55c27222252e875e4f8154a645f897168c0b1bfc612eabf785769bb34aa7984dc7e2ea2764cae8307d8c17154d7ee5f64a51a44a602c249054157dc02cd5f5c0e55fbef8519fbe327f0b1511692c5a06f19d18385f5c4dbc2d6b93f68cc2979c70e18ab93866b3bd5db8999552a0e3b4c99df58fb918bedc182ba35e003c1b4b10dd244a8ee24fffd333872ab5221985edab0fc0d0b145b6aa192858e79020103a381d93081d6301d0603551d0e04160414c77d8cc2211756259a7fd382df6be398e4d786a53081a60603551d2304819e30819b8014c77d8cc2211756259a7fd382df6be398e4d786a5a178a4763074310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e205669657731143012060355040a130b476f6f676c6520496e632e3110300e060355040b1307416e64726f69643110300e06035504031307416e64726f6964820900c2e08746644a308d300c0603551d13040530030101ff300d06092a864886f70d010104050003820101006dd252ceef85302c360aaace939bcff2cca904bb5d7a1661f8ae46b2994204d0ff4a68c7ed1a531ec4595a623ce60763b167297a7ae35712c407f208f0cb109429124d7b106219c084ca3eb3f9ad5fb871ef92269a8be28bf16d44c8d9a08e6cb2f005bb3fe2cb96447e868e731076ad45b33f6009ea19c161e62641aa99271dfd5228c5c587875ddb7f452758d661f6cc0cccb7352e424cc4365c523532f7325137593c4ae341f4db41edda0d0b1071a7c440f0fe9ea01cb627ca674369d084bd2fd911ff06cdbf2cfa10dc0f893ae35762919048c7efc64c7144178342f70581c9de573af55b390dd7fdb9418631895d5f759f30112687ff621410c069308a";
    private static final String PACKAGE_FDROID = "org.fdroid.fdroid";
    private static final String HASH_FDROID =
            "3082035e30820246a00302010202044c49cd00300d06092a864886f70d01010505003071310b300906035504061302554b3110300e06035504081307556e6b6e6f776e3111300f0603550407130857657468657262793110300e060355040a1307556e6b6e6f776e3110300e060355040b1307556e6b6e6f776e311930170603550403131043696172616e2047756c746e69656b73301e170d3130303732333137313032345a170d3337313230383137313032345a3071310b300906035504061302554b3110300e06035504081307556e6b6e6f776e3111300f0603550407130857657468657262793110300e060355040a1307556e6b6e6f776e3110300e060355040b1307556e6b6e6f776e311930170603550403131043696172616e2047756c746e69656b7330820122300d06092a864886f70d01010105000382010f003082010a028201010096d075e47c014e7822c89fd67f795d23203e2a8843f53ba4e6b1bf5f2fd0e225938267cfcae7fbf4fe596346afbaf4070fdb91f66fbcdf2348a3d92430502824f80517b156fab00809bdc8e631bfa9afd42d9045ab5fd6d28d9e140afc1300917b19b7c6c4df4a494cf1f7cb4a63c80d734265d735af9e4f09455f427aa65a53563f87b336ca2c19d244fcbba617ba0b19e56ed34afe0b253ab91e2fdb1271f1b9e3c3232027ed8862a112f0706e234cf236914b939bcf959821ecb2a6c18057e070de3428046d94b175e1d89bd795e535499a091f5bc65a79d539a8d43891ec504058acb28c08393b5718b57600a211e803f4a634e5c57f25b9b8c4422c6fd90203010001300d06092a864886f70d0101050500038201010008e4ef699e9807677ff56753da73efb2390d5ae2c17e4db691d5df7a7b60fc071ae509c5414be7d5da74df2811e83d3668c4a0b1abc84b9fa7d96b4cdf30bba68517ad2a93e233b042972ac0553a4801c9ebe07bf57ebe9a3b3d6d663965260e50f3b8f46db0531761e60340a2bddc3426098397fda54044a17e5244549f9869b460ca5e6e216b6f6a2db0580b480ca2afe6ec6b46eedacfa4aa45038809ece0c5978653d6c85f678e7f5a2156d1bedd8117751e64a4b0dcd140f3040b021821a8d93aed8d01ba36db6c82372211fed714d9a32607038cdfd565bd529ffc637212aaa2c224ef22b603eccefb5bf1e085c191d4b24fe742b17ab3f55d4e6f05ef";
    private static final String URL_FDROID = "https://f-droid.org/FDroid.apk";
    private static final String URL_FDROID_ASC =
            "https://f-droid.org/FDroid.apk.asc";
    private final Context app;
    private File apk;
    private File asc;

    /**
     * Standard constructor.
     *
     * @param ctxt Any context will do -- FDroidInstaller holds onto the
     *             Application singleton
     */
    public FDroidInstaller(@NonNull Context ctxt) {
        this.app = ctxt.getApplicationContext();
    }

    private static boolean iCanHazSpongyCastle() {
        boolean result = false;

        try {
            Class.forName("org.spongycastle.openpgp.PGPSignature");
            result = true;
        } catch (ClassNotFoundException e) {
            // guess not
        }

        return (result);
    }

    private static boolean verifySignature(File apkFile, File sigFile, InputStream keyIn)
            throws IOException, PGPException {
        InputStream apkIn = new BufferedInputStream(new FileInputStream(apkFile));
        InputStream sigIn = new BufferedInputStream(new FileInputStream(sigFile));
        boolean result = false;

        try {
            InputStream decoderIn = PGPUtil.getDecoderStream(sigIn);
            JcaPGPObjectFactory pgpFact = new JcaPGPObjectFactory(decoderIn);
            PGPSignatureList p3;
            Object o = pgpFact.nextObject();

            if (o instanceof PGPCompressedData) {
                PGPCompressedData c1 = (PGPCompressedData) o;

                pgpFact = new JcaPGPObjectFactory(c1.getDataStream());
                p3 = (PGPSignatureList) pgpFact.nextObject();
            } else {
                p3 = (PGPSignatureList) o;
            }

            InputStream decoderKeyIn = PGPUtil.getDecoderStream(keyIn);
            PGPPublicKeyRingCollection ring =
                    new PGPPublicKeyRingCollection(decoderKeyIn,
                            new JcaKeyFingerprintCalculator());
            PGPSignature sig = p3.get(0);
            PGPPublicKey key = ring.getPublicKey(sig.getKeyID());

            sig.init(new JcaPGPContentVerifierBuilderProvider().setProvider("BC"), key);

            byte[] buffer = new byte[16384];
            int sizeRead;

            while ((sizeRead = apkIn.read(buffer)) != -1) {
                sig.update(buffer, 0, sizeRead);
            }

            result = sig.verify();
        } finally {
            apkIn.close();
            sigIn.close();
        }

        return (result);
    }

    /**
     * Indicates whether a valid copy of the Play Store is installed or not
     *
     * @return status of the Play Store app
     */
    public
    @NonNull
    PackageStatus getPlayStoreStatus() {
        return (getPackageStatus(PACKAGE_PLAY_STORE, HASH_PLAY_STORE));
    }

    /**
     * Indicates whether a valid copy of F-Droid is installed or not
     *
     * @return status of the F-Droid app
     */
    public
    @NonNull
    PackageStatus getFDroidStatus() {
        return (getPackageStatus(PACKAGE_FDROID, HASH_FDROID));
    }

    /**
     * @return true if neither a valid Play Store or a valid F-Droid are
     * installed
     */
    public boolean isFDroidNeeded() {
        return (getPlayStoreStatus() != PackageStatus.VALID &&
                getFDroidStatus() != PackageStatus.VALID);
    }

    /**
     * Download the F-Droid APK using the supplied strategy. The APK will
     * be downloaded to internal storage on Android 7.0+ and external storage
     * on older devices.
     *
     * @param strategy the means for downloading the APK
     * @param cb       to find out when the APK is downloaded or if a problem arose
     *                 during the download
     */
    public void download(@NonNull final FileDownloadStrategy strategy,
                         @NonNull final FileDownloadStrategy.Callback cb) {
        if (iCanHazSpongyCastle()) {
            strategy.download(URL_FDROID_ASC, buildAscFile(),
                    new FileDownloadStrategy.Callback() {
                        @Override
                        public void onSuccess(@NonNull String url) {
                            strategy.download(URL_FDROID, buildDownloadFile(), cb);
                        }

                        @Override
                        public void onError(@NonNull String url, @NonNull Throwable t) {
                            cb.onError(url, t);
                        }
                    });
        } else {
            strategy.download(URL_FDROID, buildDownloadFile(), cb);
        }
    }

    /**
     * Given a previous successful download(), installs F-Droid from the downloaded
     * APK.
     *
     * @return true if the installation has begun, false if there is no valid
     * APK to install from
     */
    public boolean install() throws IOException, PGPException {
        if (apk != null && apk.exists() && validateAPK() == PackageStatus.VALID) {
            if (asc == null || verifySignature()) {
                app.startActivity(buildInstallIntent());

                return (true);
            }
        }

        return (false);
    }

    /**
     * Given a previous successful download(), creates an activity PendingIntent
     * to use for installing the APK (e.g., to use in a Notification).
     *
     * @param reqCode a unique request code; passed as second parameter to
     *                PendingIntent.getActivity()
     * @return a PendingIntent, or null if there is no valid APK to install from
     */
    public PendingIntent buildPendingIntent(int reqCode) {
        if (apk != null && apk.exists() && validateAPK() == PackageStatus.VALID) {
            return (PendingIntent.getActivity(app, reqCode, buildInstallIntent(), 0));
        }

        return (null);
    }

    /**
     * Delete the downloaded APK file, if it is known and exists
     */
    public void cleanup() {
        if (apk != null && apk.exists()) {
            apk.delete();
        }
    }

    private PackageStatus getPackageStatus(String pkg, String sig_hash) {
        PackageManager pm = app.getPackageManager();

        try {
            return (validateSignature(pm.getPackageInfo(pkg,
                    PackageManager.GET_SIGNATURES), sig_hash));
        } catch (PackageManager.NameNotFoundException e) {
            return (PackageStatus.NOT_INSTALLED);
        }
    }

    private PackageStatus validateAPK() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
            PackageManager pm = app.getPackageManager();

            return (validateSignature(pm.getPackageArchiveInfo(apk.getAbsolutePath(),
                    PackageManager.GET_SIGNATURES), HASH_FDROID));
        }

        return (PackageStatus.VALID);
    }

    private PackageStatus validateSignature(PackageInfo pi, String sig_hash) {
        for (Signature sig : pi.signatures) {
            String hash = sig.toCharsString();

            if (sig_hash.equals(hash)) {
                return (PackageStatus.VALID);
            }
        }

        return (PackageStatus.INVALID);
    }

    private File buildDownloadFile() {
        File dir = new File(app.getCacheDir(), SUBDIR_FDROID);

        dir.mkdirs();
        dir.setReadable(true, false);
        dir.setExecutable(true, false);

        apk = new File(dir, FILENAME_FDROID);

        if (apk.exists()) {
            apk.delete();
        }

        return (apk);
    }

    private File buildAscFile() {
        File dir = new File(app.getCacheDir(), SUBDIR_FDROID);

        dir.mkdirs();
        dir.setReadable(true, false);
        dir.setExecutable(true, false);

        asc = new File(dir, FILENAME_FDROID_ASC);

        if (asc.exists()) {
            asc.delete();
        }

        return (asc);
    }

    private boolean canInstallFromContent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            PackageManager pm = app.getPackageManager();
            Intent i = new Intent(Intent.ACTION_INSTALL_PACKAGE)
                    .setDataAndType(Uri.parse("content://foo"), MIME_TYPE_APK);

            return (pm.resolveActivity(i, PackageManager.MATCH_DEFAULT_ONLY) != null);
        }

        return (false);
    }

    private Intent buildInstallIntent() {
        Intent i;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            i = new Intent(Intent.ACTION_INSTALL_PACKAGE);
        } else {
            i = new Intent(Intent.ACTION_VIEW);
        }

        if (canInstallFromContent()) {
            final String authority = app.getPackageName() + ".fdroid.getfdroid.provider";
            final Uri uri = new Uri.Builder()
                    .scheme(ContentResolver.SCHEME_CONTENT)
                    .authority(authority)
                    .build();

            i.setDataAndType(uri, MIME_TYPE_APK)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
            apk.setReadable(true, false);

            i.setDataAndType(Uri.fromFile(apk), MIME_TYPE_APK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        return (i);
    }

    private boolean verifySignature() throws IOException, PGPException {
        return (verifySignature(apk, asc, app.getAssets().open("fdroid/admin@f-droid.org.pgp")));
    }

    /**
     * Indicates the status of some package
     */
    public enum PackageStatus {
        /**
         * The package is valid
         */
        VALID,
        /**
         * The package is invalid (e.g., has the wrong signature)
         */
        INVALID,
        /**
         * The package is not installed (or, for checking an APK file, the APK file
         * does not exist)
         */
        NOT_INSTALLED;
    }
}
