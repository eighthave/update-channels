/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.getfdroid;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * ContentProvider for serving F-Droid APK to installer on Android 7.0+,
 * because file:// Uri schemes are banned if your targetSdkVersion is 24
 * or higher.
 */
public class APKProvider extends ContentProvider {
    private final static String[] OPENABLE_PROJECTION = {
            OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE};

    @Override
    public boolean onCreate() {
        return (true);
    }

    @Override
    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode)
            throws FileNotFoundException {
        File apk = getAPK();

        if (apk.exists()) {
            return (ParcelFileDescriptor.open(apk, ParcelFileDescriptor.MODE_READ_ONLY));
        }

        throw new FileNotFoundException(uri.getPath());
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return (FDroidInstaller.MIME_TYPE_APK);
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        if (projection == null) {
            projection = OPENABLE_PROJECTION;
        }

        final MatrixCursor cursor = new MatrixCursor(projection, 1);

        MatrixCursor.RowBuilder b = cursor.newRow();

        for (String col : projection) {
            if (OpenableColumns.DISPLAY_NAME.equals(col)) {
                b.add("FDroid.apk");
            } else if (OpenableColumns.SIZE.equals(col)) {
                b.add(getAPK().length());
            } else { // unknown, so just add null
                b.add(null);
            }
        }

        return (cursor);
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues initialValues) {
        throw new RuntimeException("Operation not supported");
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String where,
                      String[] whereArgs) {
        throw new RuntimeException("Operation not supported");
    }

    @Override
    public int delete(@NonNull Uri uri, String where, String[] whereArgs) {
        throw new RuntimeException("Operation not supported");
    }

    private File getAPK() {
        File dir = new File(getContext().getCacheDir(), FDroidInstaller.SUBDIR_FDROID);

        return (new File(dir, FDroidInstaller.FILENAME_FDROID));
    }
}