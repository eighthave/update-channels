/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import java.util.HashMap;
import java.util.Map;

/**
 * Contents of an F-Droid catalog.
 * <p>
 * The install value is only populated from XML-formatted catalogs, as it does
 * not seem to appear in JSON-formatted catalogs.
 */
public class Catalog {
    private Repo repo;
    private Install install;
    private Map<String, Application> apps = new HashMap<>();

    public Repo repo() {
        return (repo);
    }

    void repo(Repo repo) {
        this.repo = repo;
    }

    public Install install() {
        return (install);
    }

    void install(Install install) {
        this.install = install;
    }

    public Map<String, Application> apps() {
        return (apps);
    }

    void addApp(Application app) {
        apps.put(app.id(), app);
    }
}
