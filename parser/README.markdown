F-Droid Catalog Parser
======================
This library contains a pair of classes &mdash; inheriting from a common abstract
class &mdash; for parsing an F-Droid repository catalog, given an `InputStream`
on the catalog's contents (e.g., [from a `StreamDownloadStrategy`](../../common/README.markdown)).

There are two concrete implementations:

- `XppCatalogParser` parses an XML-formatted F-Droid catalog

- `JsonCatalogParser` parses a JSON-formatted F-Droid catalog, but requires
API Level 11+, as it relies upon the Android SDK's `JsonReader`

To use a parser, create an instance and call `parse()` on it, providing
the `InputStream`. You get back a `Catalog` object with the contents of the
catalog.

There are three flavors of the `parse()` method. All take the `InputStream`,
and one only takes the `InputStream`. That one will parse the entire catalog.
The other two allow you to filter what is parsed to only those applications
that are of interest to you:

- `parse(InputStream, String...)` takes one or more application IDs (a.k.a.,
package names) and only parses those

- `parse(InputStream, CatalogParser.ApplicationFilter)` takes an `ApplicationFilter`
implementation &mdash; its `isAcceptable()` method returns a `boolean`
indicating if you want the supplied `Application` to be included in the
output or not

