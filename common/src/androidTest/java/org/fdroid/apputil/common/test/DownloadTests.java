/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common.test;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import junit.framework.Assert;
import org.fdroid.apputil.common.BuildConfig;
import org.fdroid.apputil.common.FileDownloadStrategy;
import org.fdroid.apputil.common.ManagedDownloadStrategy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RunWith(AndroidJUnit4.class)
public class DownloadTests {
  private CountDownLatch responseLatch;
  private Throwable testError;
  private File root;

  @Before
  public void setUp() {
    responseLatch=new CountDownLatch(1);
    root=new File(InstrumentationRegistry.getTargetContext().getExternalCacheDir(), "test");
    root.mkdirs();
  }

  @After
  public void tearDown() throws IOException {
    delete(root, false);
  }

  @Test
  public void managedSuccess() throws Throwable {
    testSuccess(new ManagedDownloadStrategy(InstrumentationRegistry.getTargetContext()));
  }

  @Test
  public void managedFailure() throws Throwable {
    testFailure(new ManagedDownloadStrategy(InstrumentationRegistry.getTargetContext()));
  }

  private void testSuccess(FileDownloadStrategy strategy) throws Throwable {
    File out=new File(root, "FDroid.apk");

    strategy.download(BuildConfig.TEST_URL_SUCCESS, out,
      new FileDownloadStrategy.Callback() {
        @Override
        public void onSuccess(String url) {
          responseLatch.countDown();
        }

        @Override
        public void onError(String url, Throwable t) {
          testError=t;
          responseLatch.countDown();
        }
      });

    responseLatch.await(60, TimeUnit.SECONDS);

    if (testError!=null) {
      throw testError;
    }

    Assert.assertTrue(out.exists());
    Assert.assertEquals(4795486, out.length());
  }

  private void testFailure(FileDownloadStrategy strategy) throws Throwable {
    File out=new File(root, "FDroid.apk");

    strategy.download(BuildConfig.TEST_URL_FAILURE, out,
      new FileDownloadStrategy.Callback() {
        @Override
        public void onSuccess(String url) {
          responseLatch.countDown();
        }

        @Override
        public void onError(String url, Throwable t) {
          testError=t;
          responseLatch.countDown();
        }
      });

    responseLatch.await(60, TimeUnit.SECONDS);

    Assert.assertNotNull(testError);
  }

  private void delete(File f, boolean deleteDir) throws IOException {
    if (f.isDirectory()) {
      for (File child : f.listFiles()) {
        delete(child, true);
      }
    }

    if (deleteDir) {
      f.delete();
    }
  }
}
